package AbstractDemo;

public class TesterBank {

	public static void main(String[] args) {
		BankA obA= new BankA();
		BankB obB= new BankB();
		BankC obC= new BankC();
		System.out.println("Bank A:  Balance="+obA.getBalance());
		System.out.println("Bank B:  Balance="+obB.getBalance());
		System.out.println("Bank C:  Balance="+obC.getBalance());
	}

}
